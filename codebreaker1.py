#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 10:37:16 2024

@author: fred
"""

import random
import common
import itertools


def init():
    """
    Une fonction qui ne fait rien... pour cette version triviale.
    Pour vos codebreaker plus avancés, c'est ici que vous pouvez initialiser
    un certain nombre de variables à chaque début de partie.
    """
    global allcombs
    allcombs = []
    return

# Q4
def codebreaker(evaluation_p):
    global allcombs
    while True:
        comb = random.choices(common.COLORS, k=common.LENGTH)
        if comb not in allcombs:
            allcombs.append(comb)
            return comb
    
