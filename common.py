#!/usr/bin/env python3
import itertools


def setLength(length=4):
    global LENGTH
    LENGTH = length
    
def setColors(colors=['R', 'V', 'B', 'J', 'N', 'M', 'O', 'G']):
    global COLORS
    COLORS = colors
    
# setLength()
# setColors()


# Notez que vos programmes doivent continuer à fonctionner si on change les valeurs par défaut ci-dessus


# Q1
def evaluation(comb, ref):
    red, white = 0, 0
    seen = [False for i in range(len(ref))]
    for i, x in enumerate(comb):
        if ref[i] == x and not seen[i]:
            red += 1
            seen[i] = True
        elif x in ref:
            pos_in_ref = ref.index(x)
            if not seen[pos_in_ref]:
                white += 1
                seen[pos_in_ref] = True
    return red, white

# Q5        
def donner_possibles(comb_testee, evalu):
    '''Prend en argument la combinaison testée et son évaluation associée
    Renvoie l'ensemble (set) des combinaisons possibles'''
    
    s=set()
    possibles = itertools.permutations(COLORS, 4)
    for i in possibles:
        if evaluation(comb_testee, "".join(i))== evalu:
            s.add("".join(i))
    return s

print(donner_possibles('BVGR', (1,1)))


ref = [0, 1, 2, 3]


comb = [0, 1, 2, 3]
print(evaluation(comb, ref))
comb = [0, 1, 2, 4]
print(evaluation(comb, ref))
comb = [0, 1, 2, 0]
print(evaluation(comb, ref))
comb = [3, 2, 1, 0]
print(evaluation(comb, ref))
comb = [3, 3, 3, 0]
print(evaluation(comb, ref))